#!/usr/bin/env python

from cycler import cycler
import matplotlib as mpl
mpl.use('agg')
import numpy as np
fontsize = 10
plot_params= {
    "axes.axisbelow":                True,
    "axes.formatter.use_mathtext":   False,
    "axes.labelsize":                fontsize,
    "axes.linewidth":                0.5 ,
    "axes.prop_cycle":               ( cycler( 'color', [ '#000000', '#a6cee3', '#1f78b4', '#b2df8a', '#33a02c' ] )
                                     + cycler( 'marker', [ '', 'o', 's', 'D', 'd' ] ) ),
    "axes.titlesize":                fontsize,
    "errorbar.capsize":              1,
    "figure.autolayout":             True,
    "font.family":                   "serif",
    "font.monospace":                [],
    "font.sans-serif":               [],
    "font.serif":                    [ 'Times New Roman' ],
    "font.size":                     fontsize,
    "font.stretch":                  'normal',
    "font.style":                    'normal',
    "font.variant":                  'normal',
    "font.weight":                   'normal',
    "grid.alpha":                    1.00,
    "grid.color":                    '#aaaaaa',
    "grid.linestyle":                ':',
    "grid.linewidth":                0.50,
    "legend.fontsize":               fontsize,
    "lines.markeredgewidth":         0,
    "lines.markersize":              2,
    "lines.linewidth":               0.50,
    "mathtext.fontset":              'custom',
    "mathtext.rm":                   'Times New Roman',
    "mathtext.it":                   'Times New Roman:italic',
    "patch.linewidth":               0.50,
    "savefig.dpi":                   600,
    "xtick.labelsize":               fontsize,
    "ytick.labelsize":               fontsize,
    }
mpl.rcParams.update( plot_params )

class plotter:
    ''' This class prepares plots using Python's matplotlib that are consistent
    in style with pdflatex output. Also, some functions are pre-packaged for
    common plots.  Recommended output formats are png or PDF.

    Substantially from: http://bkanuka.com/articles/native-latex-plots/ '''

    import matplotlib.pyplot as plt

    def __init__( self, figure_width = 6.5 ):
        '''Construct a figure and axis assuming a width of 6.5 inches.'''
        self.plt.clf()
        golden_ratio = ( np.sqrt( 5.0 ) - 1.0 ) / 2.0
        self.fig = self.plt.figure( figsize = [ figure_width,
                                                figure_width * golden_ratio ] )
        self.ax = self.fig.add_subplot( 111 )
        return

    def plot_xy( self, x, y,
        logX = False,
        logY = False,
        xlabel = None,
        ylabel = None,
        title = None,
        verbose = True,):

        if( logX ):
            self.plt.xscale('log')
        if( logY ):
            self.plt.yscale('log')

        self.plt.plot(x, y)

        # self.ax.set_xlim([1e+0 , 1e8])
        # self.ax.set_ylim([0, 1e6])
        # self.ax.grid(which='both')

        if(title != None):
            self.plt.title(title)
        if(xlabel != None):
            self.plt.xlabel(xlabel)
        if(ylabel != None):
            self.plt.ylabel(ylabel)

        return

    def plot_xy_step( self, x, y,
        logX = False,
        logY = False,
        xlabel = None,
        ylabel = None,
        title = None,
        verbose = True,):

        if( logX ):
            self.plt.xscale('log')
        if( logY ):
            self.plt.yscale('log')

        self.plt.step(x, y, where='post', marker='')

        # self.ax.set_xlim([1e+0 , 1e8])
        # self.ax.set_ylim([0, 1e6])
        # self.ax.grid(which='both')

        if(title != None):
            self.plt.title(title)
        if(xlabel != None):
            self.plt.xlabel(xlabel)
        if(ylabel != None):
            self.plt.ylabel(ylabel)

        return

    def plot_histogram( self, x,
        nbins=100,
        xmin = None,
        xmax = None,
        logX=False,
        logY=False,
        xlabel=None,
        ylabel=None,
        title=None,
        verbose=True,
        showmean=True,
        normed=False):
        # Load values x to plot a histogram of with a command line:
        #   x, amfp = np.loadtxt(infilename, usecols=(0, 8), unpack=True)

        if( xmin == None ):
            xmin = np.min( x[np.nonzero( x ) ] )
        if( xmax == None ):
            xmax = np.max( x)
        if( logX ):
            xbins = np.logspace( np.log10( xmin ), np.log10( xmax ), nbins )
            self.plt.xscale( 'log' )
        else:
            xbins = np.linspace( xmin, xmax, nbins )

        if( logY ):
            self.plt.yscale( 'log' )

        n, bins, patches = self.plt.hist( x,
                                          bins = xbins,
                                          facecolor = '#000000',
                                          edgecolor = 'none',
                                          alpha = 0.25,
                                          histtype = 'stepfilled',
                                          log = logY,
                                          normed = normed )

        # self.ax.set_xlim([1e+0 , 1e8])
        # self.ax.set_ylim([0, 1e6])
        # self.ax.grid(which='both')

        # Add line for the mean value of the distribution.
        if( showmean ):
            xmean = np.mean( x )
            xmedian = np.median( x )
            self.plt.axvline( xmean, color='k', linestyle='--', linewidth=0.5 )
            ymin, ymax = self.ax.get_ylim()
            self.plt.text( 0.95 * xmean, 0.95 * ymax,
                'Mean = ' + '{0:.2e}'.format( xmean ),
                verticalalignment = 'top',
                horizontalalignment = 'right',
                rotation = 'vertical' )

        if(title != None):
            self.plt.title(title)
        if(xlabel != None):
            self.plt.xlabel(xlabel)
        if(ylabel != None):
            self.plt.ylabel(ylabel)

        if(verbose):
            print('+' + 59*'-')
            print('| {:>40}: {:12.5e}'.format('Min Histogram Val', np.min(x[np.nonzero(x)])))
            print('| {:>40}: {:12.5e}'.format('Max Histogram Val', np.max(x)))
            print('| {:>40}: {:12.5e}'.format('Mean', xmean))
            print('| {:>40}: {:12.5e}'.format('Median', xmedian))
            print('+' + 59*'-')

        return

    # A function to rasterize components of a matplotlib figure while keeping
    # axes, labels, etc as vector components
    # from: https://brushingupscience.wordpress.com/2017/05/09/vector-and-raster-in-one-with-matplotlib/
    # and https://gist.github.com/hugke729/78655b82b885cde79e270f1c30da0b5f
    def rasterize_and_save( self, fname, rasterize_list=None, fig=None, dpi=None,
                           savefig_kw={} ):
        from inspect import getmembers, isclass
        import matplotlib
        import matplotlib.pyplot as plt
        import numpy as np
        """Save a figure with raster and vector components
        This function lets you specify which objects to rasterize at the export
        stage, rather than within each plotting call. Rasterizing certain
        components of a complex figure can significantly reduce file size.
        Inputs
        ------
        fname : str
            Output filename with extension
        rasterize_list : list (or object)
            List of objects to rasterize (or a single object to rasterize)
        fig : matplotlib figure object
            Defaults to current figure
        dpi : int
            Resolution (dots per inch) for rasterizing
        savefig_kw : dict
            Extra keywords to pass to matplotlib.pyplot.savefig
        If rasterize_list is not specified, then all contour, pcolor, and
        collects objects (e.g., ``scatter, fill_between`` etc) will be
        rasterized
        Note: does not work correctly with round=True in Basemap
        Example
        -------
        Rasterize the contour, pcolor, and scatter plots, but not the line
        >>> import matplotlib.pyplot as plt
        >>> from numpy.random import random
        >>> X, Y, Z = random((9, 9)), random((9, 9)), random((9, 9))
        >>> fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(ncols=2, nrows=2)
        >>> cax1 = ax1.contourf(Z)
        >>> cax2 = ax2.scatter(X, Y, s=Z)
        >>> cax3 = ax3.pcolormesh(Z)
        >>> cax4 = ax4.plot(Z[:, 0])
        >>> rasterize_list = [cax1, cax2, cax3]
        >>> rasterize_and_save( 'out.pdf', rasterize_list, fig=fig, dpi=300, savefig_kw = dict( bbox_inches = 'tight' ) )
        """

        # Behave like pyplot and act on current figure if no figure is specified
        fig = plt.gcf() if fig is None else fig

        # Need to set_rasterization_zorder in order for rasterizing to work
        zorder = -5  # Somewhat arbitrary, just ensuring less than 0

        if rasterize_list is None:
            # Have a guess at stuff that should be rasterised
            types_to_raster = ['QuadMesh', 'Contour', 'collections']
            rasterize_list = []

            print("""
            No rasterize_list specified, so the following objects will
            be rasterized: """)
            # Get all axes, and then get objects within axes
            for ax in fig.get_axes():
                for item in ax.get_children():
                    if any(x in str(item) for x in types_to_raster):
                        rasterize_list.append(item)
            print('\n'.join([str(x) for x in rasterize_list]))
        else:
            # Allow rasterize_list to be input as an object to rasterize
            if type(rasterize_list) != list:
                rasterize_list = [rasterize_list]

        for item in rasterize_list:

            # Whether or not plot is a contour plot is important
            is_contour = (isinstance(item, matplotlib.contour.QuadContourSet) or
                          isinstance(item, matplotlib.tri.TriContourSet))

            # Whether or not collection of lines
            # This is commented as we seldom want to rasterize lines
            # is_lines = isinstance(item, matplotlib.collections.LineCollection)

            # Whether or not current item is list of patches
            all_patch_types = tuple(
                x[1] for x in getmembers(matplotlib.patches, isclass))
            try:
                is_patch_list = isinstance(item[0], all_patch_types)
            except TypeError:
                is_patch_list = False

            # Convert to rasterized mode and then change zorder properties
            if is_contour:
                curr_ax = item.ax.axes
                curr_ax.set_rasterization_zorder(zorder)
                # For contour plots, need to set each part of the contour
                # collection individually
                for contour_level in item.collections:
                    contour_level.set_zorder(zorder - 1)
                    contour_level.set_rasterized(True)
            elif is_patch_list:
                # For list of patches, need to set zorder for each patch
                for patch in item:
                    curr_ax = patch.axes
                    curr_ax.set_rasterization_zorder(zorder)
                    patch.set_zorder(zorder - 1)
                    patch.set_rasterized(True)
            else:
                # For all other objects, we can just do it all at once
                curr_ax = item.axes
                curr_ax.set_rasterization_zorder(zorder)
                item.set_rasterized(True)
                item.set_zorder(zorder - 1)

        # dpi is a savefig keyword argument, but treat it as special since it is
        # important to this function
        if dpi is not None:
            savefig_kw['dpi'] = dpi

        # Save resulting figure
        fig.savefig(fname, **savefig_kw)

import __main__ as main
if(__name__ == '__main__' and hasattr(main, '__file__')):

    from plotter import plotter

    import numpy as np

    x = np.linspace( 0, 2 )
    y = x * x

    p = plotter( )
    p.plt.plot( x, y )
    # p.plot_xy( x, y )
    # p.plot_xy_step( x, y )
    # p.plot_histogram( y )
    p.ax.set_xlabel( '$x$-axis' )
    p.ax.set_ylabel( '$y$-axis' )
    # p.ax.grid()
    p.plt.savefig( 'test.png' )

