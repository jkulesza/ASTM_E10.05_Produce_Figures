#!/usr/bin/env python

import numpy as np

class xs_data_exp:

    def __init__( self, infilename ):
        self.infilename = infilename
        self.read_zvd_exp_file()
        return

    def read_zvd_exp_file( self ):
        import re

        # Read in associated filename for processing.
        with open(self.infilename, 'r') as myfile:
            infile = myfile.read()

        # Get header data.
        self.name = re.search(r'name:\s+(.*?)\n', infile, re.S).group(1)
        self.Xaxis = re.search(r'X.axis:\s+(.*?)\n', infile, re.S).group(1)
        self.Yaxis = re.search(r'Y.axis:\s+(.*?)\n', infile, re.S).group(1)
        self.wdata = int(re.search(r'wdata:\s+(.*?)\n', infile, re.S).group(1))
        self.ldata = int(re.search(r'ldata:\s+(.*?)\n', infile, re.S).group(1))

        # Get field and field units.
        tmp = re.search(r'data\.\.\..*?\n(.*?)\n(.*?)\n', infile, re.S)
        self.fields = tmp.group(1)
        self.field_units = tmp.group(2)

        self.fields = self.fields.split()[1:]
        self.field_units = self.field_units.split()[1:]

        self.xlabel = self.Xaxis + ' [' + self.field_units[0] + ']'
        self.ylabel = self.Yaxis + ' [' + self.field_units[2] + ']'

        # Get data.
        tmp = re.sub(r'#.*?\n', '', infile)
        tmp = re.sub(r'\/\/\n', '', tmp, re.S)
        tmp = tmp.split()
        self.X  = np.array( [ float( x ) for x in tmp[ 0::4 ] ] )
        self.dX = np.array( [ float( x ) for x in tmp[ 1::4 ] ] )
        self.Y  = np.array( [ float( x ) for x in tmp[ 2::4 ] ] )
        self.dY = np.array( [ float( x ) for x in tmp[ 3::4 ] ] )

        return

class xs_data_eval:

    def __init__( self, infilename ):
        self.infilename = infilename
        self.read_zvd_eval_file()
        return

    def read_zvd_eval_file( self ):
        import re

        # Read in associated filename for processing.
        with open( self.infilename, 'r' ) as myfile:
            infile = myfile.read()

        # Get header data.
        self.name = re.search(r'name:\s+(.*?)\n', infile, re.S).group(1)
        self.Xaxis = re.search(r'X.axis:\s+(.*?)\n', infile, re.S).group(1)
        self.Yaxis = re.search(r'Y.axis:\s+(.*?)\n', infile, re.S).group(1)
        self.wdata = int(re.search(r'wdata:\s+(.*?)\n', infile, re.S).group(1))
        self.ldata = int(re.search(r'ldata:\s+(.*?)\n', infile, re.S).group(1))

        # Get field and field units.
        tmp = re.search(r'data\.\.\..*?\n(.*?)\n(.*?)\n', infile, re.S)
        self.fields = tmp.group(1)
        self.field_units = tmp.group(2)

        self.fields = self.fields.split()[1:]
        self.field_units = self.field_units.split()[1:]

        self.xlabel = self.Xaxis + ' [' + self.field_units[0] + ']'
        self.ylabel = self.Yaxis + ' [' + self.field_units[1] + ']'

        # Get data.
        tmp = re.sub(r'#.*?\n', '', infile, re.S)
        tmp = re.sub(r'\/\/\n', '', tmp, re.S)
        tmp = tmp.split()
        self.X = np.array( [ float( x ) for x in tmp[ 0::2 ] ] )
        self.Y = np.array( [ float( x ) for x in tmp[ 1::2 ] ] )

        return

class xs_data_exp_eval:
    '''
This class is a hack to enable reading conjoined experimental-evaluated nuclear
data files as exported from the EXFOR website.'''

    def __init__( self, infilename ):
        '''
The 'hack' is to split the conjoined file into multiple parts to then process
individually using the previous classes.  Naturally, this should be done better
at some point.'''

        import re

        # from: https://stackoverflow.com/a/12717497
        def files():
            n = 0
            while True:
                n += 1
                yield open('./{:02d}.part'.format( n ), 'w')

        pat = '//'
        fs = files()
        outfile = next(fs)

        with open(infilename) as infile:
            for line in infile:
                if pat not in line:
                    outfile.write(line)
                else:
                    items = line.split(pat)
                    outfile.write(items[0])
                    for item in items[1:]:
                        outfile = next(fs)
                        outfile.write(pat + item)

        self.exp = xs_data_exp( '01.part' )
        self.eval = xs_data_eval( '02.part' )

        import glob
        import os
        for f in glob.glob( './*.part' ):
            os.remove( f )

        return

