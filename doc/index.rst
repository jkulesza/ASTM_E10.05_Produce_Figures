.. ASTM E10.05 Data Collator documentation master file, created by
   sphinx-quickstart on Fri Jun 29 20:34:28 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ASTM E10.05 Data Collator's documentation!
=====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction.rst
   Retrieving_EXFOR_Data.rst
   .. E_0181.rst
   .. E_0261.rst
   .. E_0262.rst
   E_0263.rst
   E_0264.rst
   E_0266.rst
   .. E_0385.rst
   .. E_0393.rst
   .. E_0481.rst
   .. E_0482.rst
   E_0523.rst
   E_0526.rst
   E_0666.rst
   E_0693.rst
   E_0704.rst
   E_0705.rst
   E_0706.rst
   E_0844.rst
   .. E_0853.rst
   .. E_0854.rst
   .. E_0910.rst
   .. E_0944.rst
   .. E_1005.rst
   .. E_1006.rst
   .. E_1018.rst
   .. E_1035.rst
   E_1297.rst
   .. E_2005.rst
   .. E_2006.rst
   .. E_2059.rst
   .. E_2956.rst
   .. E_3063.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

