#!/usr/bin/env python
# -*- coding: utf-8 -*-


class E_0264:
    def __init__(self, destination="./doc/figures_generated"):
        self.destination = destination
        return

    def plot_figure_01(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np
        import matplotlib.ticker as ticker
        from scipy.interpolate import interp1d

        # Import the data
        infilename = "data/E_0264_Fig_01.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "355744e8a35ff66e4c1fa87ac6744508"
        )
        data = np.genfromtxt(infilename, delimiter=",", skip_header=1)
        x = data[:, 0]
        y = np.transpose(data[:, [1, 2, 3, 4]])
        x_smooth = np.linspace(x.min(), x.max(), 1000)

        # Create 2 subplots
        p = plotter.plotter(figure_width=figure_width)
        figure, (ax1, ax2) = p.plt.subplots(
            1,
            2,
            sharey=False,
            facecolor="w",
            gridspec_kw={"width_ratios": [3, 1]},
            figsize=(figure_width, figure_width / 1.61),
        )

        # Smooth and plot each set of data
        for burnup in y:
            y_smooth = interp1d(x, burnup, kind="cubic")
            ax1.plot(
                x_smooth, y_smooth(x_smooth), linestyle="-", color="k", marker="None"
            )
            ax2.plot(
                x_smooth, y_smooth(x_smooth), linestyle="--", color="k", marker="None"
            )

        # Format the y-axis to be different on left and right
        fig1_primary_y_axis = [1.0, 1.2, 1.4, 1.6, 1.8]
        ax1.set_ylabel(
            r"$\frac{\mathrm{No.\ of\ }\ ^{58}\mathrm{Co\ Atoms\ Without\ Burnup}}{\mathrm{No.\ of\ }\ ^{58}\mathrm{Co\ Atoms\ With\ Burnup}}=R$"
        )
        ax1.set_ylim(1.0, 1.8)
        ax1.set_yticks(fig1_primary_y_axis)
        ax1.yaxis.set_major_formatter(ticker.FixedFormatter(fig1_primary_y_axis))
        ax1.yaxis.tick_left()

        fig1_secondary_y_axis = [1.0, 1.2, 1.4, 1.8, 2.1, 2.5, 3.2]
        ax2.set_ylim(1.0, 3.2)
        ax2.set_yticks(fig1_secondary_y_axis)
        ax2.yaxis.set_major_formatter(ticker.FixedFormatter(fig1_secondary_y_axis))
        ax2.yaxis.tick_right()

        p.plt.gca().yaxis.set_minor_formatter(ticker.NullFormatter())

        # Breakup the x-axis between 0-100 and 400-600
        ax1.set_xlim(0, 100)
        ax2.set_xlim(200, 600)
        ax1.spines["right"].set_visible(False)
        ax2.spines["left"].set_visible(False)
        ax1.set_xticks([x for x in range(0, 120, 20)])
        ax2.set_xticks([x for x in range(200, 800, 200)])

        # x-axis label centered
        figure.text(0.5, 0.01, "Exposure Time (hours)", ha="center")

        # Gridlines
        ax1.grid(linestyle="-")
        ax2.grid(linestyle="--")

        # Annotations
        arrow = dict(
            arrowstyle="fancy",
            facecolor="black",
            connectionstyle="arc3,rad=-0.3",
            relpos=(0, 0.5),
        )
        ax1.annotate(
            (r"$\Phi_\mathrm{th} = 5 \times 10^{13}$"),
            xy=(40, 1.088),
            xytext=(56.75, 1.03),
            arrowprops=arrow,
            fontsize=8,
        )
        ax1.annotate(
            (r"$\Phi_\mathrm{th} = 1 \times 10^{14}$"),
            xy=(40, 1.158),
            xytext=(56.75, 1.118),
            arrowprops=arrow,
            fontsize=8,
        )
        ax1.annotate(
            (r"$\Phi_\mathrm{th} = 3 \times 10^{14}$"),
            xy=(40, 1.325),
            xytext=(56.75, 1.25),
            arrowprops=arrow,
            fontsize=8,
        )
        ax1.annotate(
            (r"$\Phi_\mathrm{th} = 6 \times 10^{14}$"),
            xy=(40, 1.462),
            xytext=(56.75, 1.41),
            arrowprops=arrow,
            fontsize=8,
        )

        # Text with specific data points
        # p.plt.text( 400, 1.14, '1.16', fontsize = 8, horizontalalignment = 'center', verticalalignment = 'top', bbox=dict(facecolor='white', edgecolor='None',  alpha=0.9, boxstyle='square,pad=0') )

        # Save the figure
        p.plt.savefig(self.destination + "/E_0264_Figure_01.png")
        return

    def plot_figure_02(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np
        from scipy.interpolate import interp1d

        # Import the data
        infilename = "data/E_0264_Fig_02.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "727c856fb4494285fcc355257f35a494"
        )
        data = np.genfromtxt(infilename, delimiter=";", skip_header=3)
        x = np.array(data[:, 0]) / 1e6  # Convert eV to MeV
        y = np.array(data[:, 1])
        sd = np.array(data[:, 2])
        x_smooth = np.linspace(x.min(), x.max(), 1000)
        y_smooth = interp1d(x, y, kind="cubic")
        sd_smooth = interp1d(x, sd, kind="cubic")

        # Plot the data
        p = plotter.plotter(figure_width=figure_width)
        p.plt.plot(x_smooth, y_smooth(x_smooth))
        # p.plt.plot( x_smooth, y_smooth(x_smooth) + sd_smooth(x_smooth), linewidth=0.5)
        # p.plt.plot( x_smooth, y_smooth(x_smooth) - sd_smooth(x_smooth), linewidth=0.5)
        p.plt.fill_between(
            x_smooth,
            y_smooth(x_smooth) + sd_smooth(x_smooth),
            y_smooth(x_smooth) - sd_smooth(x_smooth),
            facecolor="k",
            alpha=0.2,
        )

        # Format the plot area
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Cross Section (barns)")
        p.plt.xlim(xmin=0.0, xmax=20.0)
        p.plt.ylim(ymin=0.0, ymax=0.8)
        p.plt.xticks([x for x in range(0, 25, 5)])
        p.ax.grid()
        p.ax.minorticks_on()
        p.ax.tick_params(which="both", direction="in")

        # Add annotations
        arrow = dict(
            arrowstyle="fancy",
            facecolor="black",
            connectionstyle="arc3,rad=0.3",
            relpos=(0, 0.5),
        )
        p.plt.text(
            0.5, 0.7, ("$^{58}$Ni(n,p)$^{58}$Co IRDFF-1.05 Cross Section $\pm1\sigma$")
        )  # Source
        p.plt.annotate(
            "Threshold = 0.400 MeV", xy=(0.9, 0.01), xytext=(3, 0.05), arrowprops=arrow
        )  # Threshold

        # Save figure
        p.plt.savefig(self.destination + "/E_0264_Figure_02.png")
        return


def make_plots(destination):
    e0264 = E_0264(destination)
    print(" Generating E264, Figure 1")
    e0264.plot_figure_01()
    print(" Generating E264, Figure 2")
    e0264.plot_figure_02()


import __main__ as main

if __name__ == "__main__" and hasattr(main, "__file__"):

    make_plots()
