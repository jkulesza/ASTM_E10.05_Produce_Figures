#!/usr/bin/env python
# -*- coding: utf-8 -*-


class E_0266:
    def __init__(self, destination="./doc/figures_generated"):
        self.destination = destination
        return

    def plot_figure_01(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        from scripts_utilities.retrieve_zvd_data import xs_data_exp_eval
        import numpy as np

        infilename = "data/E_0266_Fig_01.zvd"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "0b76044511d9de12552c080ef13566cd"
        )
        data = xs_data_exp_eval(infilename)

        x_exp = data.exp.X
        x_err = data.exp.dX
        y_exp = data.exp.Y
        y_err = data.exp.dY
        x_eval = data.eval.X
        y_eval = data.eval.Y

        p = plotter.plotter(figure_width=figure_width)
        p.plt.plot(x_eval, y_eval, color="#000000", zorder=10, label="Eval. Data")
        p.plt.errorbar(
            x_exp,
            y_exp,
            xerr=x_err,
            yerr=y_err,
            fmt="s",
            markersize=2.0,
            label="Exp. Data",
        )
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Cross Section (barns)")
        p.plt.xscale("linear")
        p.plt.yscale("linear")
        p.plt.xlim(left=4, right=20)
        p.plt.ylim(bottom=0, top=0.16)
        p.plt.xticks([x for x in range(4, 20)])
        p.ax.grid()
        p.plt.legend(loc="best", fontsize=8)
        p.plt.savefig(self.destination + "/E_0266_Figure_01.png")
        return

    def plot_figure_02(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np

        infilename = "data/E_0266_Fig_02.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "019665bc04e8c592b06e3468f25104f5"
        )
        data = np.genfromtxt(infilename, delimiter=";", skip_header=4)
        x = np.array(data[:, 0])
        y = 100 * np.array(data[:, 2] / data[:, 1])

        p = plotter.plotter(figure_width=figure_width)
        p.plt.step(x, y)
        p.plt.xlabel("Neutron Energy (eV)")
        p.plt.ylabel("Standard Deviation (%)")
        p.plt.xscale("linear")
        p.plt.yscale("log")
        p.plt.xlim(left=4e6, right=20e6)
        p.plt.ylim(bottom=0.1, top=25)
        p.plt.xticks([5000000 * x for x in range(1, 4)])
        p.plt.yticks([10 ** y for y in range(-1, 1)])
        p.ax.grid()
        p.plt.savefig(self.destination + "/E_0266_Figure_02.png")
        return


def make_plots(destination):
    e0266 = E_0266(destination)
    print(" Generating E266, Figure 1")
    e0266.plot_figure_01()
    print(" Generating E266, Figure 2")
    e0266.plot_figure_02()


import __main__ as main

if __name__ == "__main__" and hasattr(main, "__file__"):

    make_plots()
