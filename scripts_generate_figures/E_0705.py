#!/usr/bin/env python
# -*- coding: utf-8 -*-


class E_0705:
    def __init__(self, destination="./doc/figures_generated"):
        self.destination = destination
        return

    def plot_figure_01(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np

        infilename = "data/E_0705_Fig_01.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "cc99a65cd9046c7ed3561f325da873a1"
        )
        data = np.genfromtxt(infilename, delimiter=";", skip_header=3)
        x = np.array(data[:, 0])
        y = np.array(data[:, 1])

        # Convert eV to MeV.
        x = x / 1e6

        p = plotter.plotter(figure_width=figure_width)
        p.plt.plot(x, y)
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Cross Section (barns)")
        p.plt.xscale("log")
        p.plt.yscale("log")
        p.plt.xlim(xmin=1e-10, xmax=2e1)
        p.plt.ylim(ymin=1e-3, ymax=1e1)
        p.plt.xticks([10 ** x for x in range(-10, 2)], fontsize=8)
        p.ax.grid()
        p.plt.savefig(self.destination + "/E_0705_Figure_01.png")
        return

    def plot_figure_02(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np

        infilename = "data/E_0705_Fig_01.csv"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "cc99a65cd9046c7ed3561f325da873a1"
        )
        data = np.genfromtxt("data/E_0705_Fig_01.csv", delimiter=";", skip_header=3)
        x = np.array(data[:, 0])
        y = 100 * np.array(data[:, 2] / data[:, 1])

        # Convert eV to MeV.
        x = x / 1e6

        p = plotter.plotter(figure_width=figure_width)
        p.plt.step(x, y)
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Standard Deviation (%)")
        p.plt.xscale("log")
        p.plt.xlim(xmin=1e-8, xmax=2e1)
        p.plt.ylim(ymin=0, ymax=70)
        p.plt.xticks([10 ** x for x in range(-10, 2)], fontsize=8)
        p.ax.grid()
        p.plt.savefig(self.destination + "/E_0705_Figure_02.png")
        return

    def plot_figure_03(self, figure_width=3.5):
        import hashlib
        import scripts_utilities.plotter as plotter
        from scripts_utilities.retrieve_zvd_data import xs_data_exp_eval
        import numpy as np

        infilename = "data/E_0705_Fig_03.zvd"
        assert (
            hashlib.md5(open(infilename, "rb").read()).hexdigest()
            == "98494ba95c0f3bf8b68558e59e7b5765"
        )
        data = xs_data_exp_eval("data/E_0705_Fig_03.zvd")

        x_exp = data.exp.X
        x_err = data.exp.dX
        y_exp = data.exp.Y
        y_err = data.exp.dY
        x_eval = data.eval.X
        y_eval = data.eval.Y

        p = plotter.plotter(figure_width=figure_width)
        p.plt.plot(x_eval, y_eval, color="#000000", zorder=10, label="Eval. Data")
        p.plt.errorbar(
            x_exp,
            y_exp,
            xerr=x_err,
            yerr=y_err,
            fmt="s",
            markersize=2.0,
            label="Exp. Data",
        )
        p.plt.xlabel("Neutron Energy (MeV)")
        p.plt.ylabel("Cross Section (barns)")
        p.plt.xscale("log")
        p.plt.yscale("log")
        p.plt.xlim(xmin=1e-10, xmax=2e1)
        p.plt.ylim(ymin=1e-3, ymax=1e1)
        p.plt.xticks([10 ** x for x in range(-10, 2)], fontsize=8)
        p.ax.grid()
        p.plt.legend(loc="best", fontsize=8)
        p.plt.savefig(self.destination + "/E_0705_Figure_03.png")
        return


def make_plots(destination):
    e0705 = E_0705(destination)
    print(" Generating E705, Figure 1")
    e0705.plot_figure_01()
    print(" Generating E705, Figure 2")
    e0705.plot_figure_02()
    print(" Generating E705, Figure 3")
    e0705.plot_figure_03()


import __main__ as main

if __name__ == "__main__" and hasattr(main, "__file__"):

    make_plots()
