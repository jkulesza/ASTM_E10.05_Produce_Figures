#!/usr/bin/env python
# -*- coding: utf-8 -*-

class E_0523( ):

    def __init__( self, destination = './doc/figures_generated' ):
        self.destination = destination
        return

    def plot_figure_01( self, figure_width = 3.5 ):
        import hashlib
        import scripts_utilities.plotter as plotter
        from scripts_utilities.retrieve_zvd_data import xs_data_exp
        import numpy as np

        infilename = 'data/E_0523_Fig_01.zvd'
        assert( hashlib.md5( open( infilename, 'rb' ).read() ).hexdigest() ==
            '14a0d328ef18bbc8eea1fd629795e126' )
        data = xs_data_exp( infilename )

        x_exp = data.X
        x_err = data.dX
        y_exp = data.Y
        y_err = data.dY

        infilename = 'data/E_0523_Fig_01.csv'
        assert( hashlib.md5( open( infilename, 'rb' ).read() ).hexdigest() ==
            '500a7694463b8260e341ef81843aab2c' )
        data2 = np.genfromtxt( infilename, delimiter = ';', skip_header = 3 )

        x = np.array( data2[:,0] )*1E-6
        y = np.array( data2[:,1] )

        p = plotter.plotter( figure_width = figure_width )
        p.plt.plot(
            x, y,
            color = '#000000',
            zorder = 10,
            label = 'Eval. Data' )
        p.plt.errorbar(
            x_exp, y_exp,
            xerr = x_err, yerr = y_err,
            fmt = 's',
            markersize = 2.0,
            label = 'Exp. Data' )
        p.plt.xlabel( 'Neutron Energy (MeV)' )
        p.plt.ylabel( 'Cross Section (barns)' )
        p.plt.xscale( 'linear' )
        p.plt.yscale( 'linear' )
        p.plt.xlim( left = 1.5, right = 20 )
        p.plt.ylim( bottom = 0, top = 0.065 )
        p.plt.xticks( [ 2*x for x in range( 1, 11 ) ] )
        p.plt.yticks( [ 0.01*y for y in range( 0, 7 ) ] )
        p.ax.grid()
        p.plt.legend( loc = 'best' )
        p.plt.savefig( self.destination + '/E_0523_Figure_01.png' )
        return

    def plot_figure_02( self, figure_width = 3.5 ):
        import hashlib
        import scripts_utilities.plotter as plotter
        import numpy as np

        infilename = 'data/E_0523_Fig_02.csv'
        assert( hashlib.md5( open( infilename, 'rb' ).read() ).hexdigest() ==
            'da1b723f74be379673a512da4d1aa348' )
        data = np.genfromtxt( infilename, delimiter = ';', skip_header = 3 )
        x = np.array( data[:,0] )
        for i in range(0,len(data[:,1])):
          if data[i,1] == 0:
            data[i,1] = 1E-30
        y = 100*np.array( data[:,2] / data[:,1] )

        p = plotter.plotter( figure_width = figure_width )
        p.plt.step( x, y ,
            where = 'post',
            label = 'RRDF-2002')
        p.plt.xlabel( 'Neutron Energy (eV)' )
        p.plt.ylabel( 'Uncertainty (%)' )
        p.plt.xscale( 'linear' )
        p.plt.yscale( 'linear' )
        p.plt.xlim( left = 0, right = 20E6 )
        p.plt.ylim( bottom = 0, top = 8 )
        p.plt.xticks( [ 5000000*x for x in range( 1, 5 ) ] )
        p.plt.yticks( [ 2*y for y in range( 0, 5 ) ] )
        p.ax.grid()
        p.plt.legend( loc = 'best' )
        p.plt.savefig( self.destination + '/E_0523_Figure_02.png' )
        return

def make_plots( destination ):
    e0523 = E_0523( destination )
    print( ' Generating E523, Figure 1' )
    e0523.plot_figure_01( )
    print( ' Generating E523, Figure 2' )
    e0523.plot_figure_02( )

import __main__ as main
if(__name__ == '__main__' and hasattr(main, '__file__')):

    make_plots( )
