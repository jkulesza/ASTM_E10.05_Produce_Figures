#!/usr/bin/env python

################################################################################
# EXECUTE PROGRAM ##############################################################
################################################################################

import __main__ as main
if(__name__ == '__main__' and hasattr(main, '__file__')):

    import argparse
    import os
    import sys
    import textwrap

    import scripts_generate_figures.E_0263 as E_0263
    import scripts_generate_figures.E_0264 as E_0264
    import scripts_generate_figures.E_0266 as E_0266
    import scripts_generate_figures.E_0523 as E_0523
    import scripts_generate_figures.E_0666 as E_0666
    import scripts_generate_figures.E_0705 as E_0705
    import scripts_generate_figures.E_1297 as E_1297

    supported_standards = {
        '263': E_0263,
        '264': E_0264,
        '266': E_0266,
        '523': E_0523,
        '666': E_0666,
        '705': E_0705,
        '1297': E_1297,
    }

    supported_standards_listing = '\n' + \
        textwrap.fill( ', '.join(
        [ '{:}'.format( k ) for k in sorted( list( supported_standards.keys() ), key = int ) ]
        ), width = 80 )

    description = textwrap.dedent(
    """
This script is used to generate figures for ASTM International E10 standards.
In general, it is run by a GitLab Docker instance, which will produce
high-resolution PNG images and a series of Sphinx-based pages that describe how
the images are produced.
    """)

    epilog = textwrap.dedent(
    """
Typical command line calls might look like:

> python """ + os.path.basename(__file__) + """

to produce all standards or

> python """ + os.path.basename(__file__) + """ -s 705

to produce only one specific standard. When selecting a specific standard,
valid entries are:
""" ) + supported_standards_listing + u"\u2063"

    parser = argparse.ArgumentParser(
        formatter_class = argparse.RawDescriptionHelpFormatter,
        description = description,
        epilog = epilog)

    # Optional named argument(s).
    parser.add_argument(
        '--verbosity', '-V', type = int, default = '1',
        help = 'degree of verbosity, from 0-2 (default: 1)')

    parser.add_argument(
        '--standard', '-s', type = str, default = 'all',
        help = 'which standard to produce (default: all)')

    outdir_default = './doc/figures_generated'
    parser.add_argument(
        '--outdir', '-o', type = str, default = outdir_default,
        help = 'which directory to send figures to (default: ' + outdir_default
        + ')')

    args = parser.parse_args()

    # Only perform matplotlib operations after argparse in case just help is
    # requested.
    import matplotlib as mpl
    mpl.use('agg')
    import matplotlib.font_manager
    import matplotlib.font_manager as mplfm

    # Print out list of font locations and cleanup font manager listing.
    # These actions are taken to workaround an issue that if Times New Roman is
    # selected, then the bold version is inappropriately loaded.  This works in
    # concert with the YAML file to delete the bold fonts explicitly.  This action
    # is taken because because of inconsistent behavior between the GitLab Docker
    # runner and local font cache updating.
    # fonts = [ f for f in matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf') ]
    # [ print( f ) for f in sorted( fonts ) ]
    mplfm._rebuild()

    # Print versions of environment components.
    print( 'Python version {:}'.format( sys.version ) )
    print( 'Matplotlib version {:}'.format( mpl.__version__ ) )

    # Generate figures.
    if( args.standard.lower() == 'all' ):
        for k,v in supported_standards.items():
            v.make_plots( destination = outdir_default )
    else:
        supported_standards[ args.standard ].make_plots(
            destination = outdir_default )

